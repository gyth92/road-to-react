import { useState, useEffect, useRef } from 'react';
import './App.css';


const useSemiPersistentState = (key, initialState) => {
  const [value, setValue] = useState(
    localStorage.getItem(key) || initialState
  )

  useEffect(() => {
    localStorage.setItem(key, value);
  }, [value, key]);

  return [value, setValue];
}


const App = () => {
  const initialStories = [
    {
      title: 'React',
      url: 'https://reactjs.org',
      author: 'Jordan Walke',
      num_comments: 3,
      points: 4,
      id: 0
    },
    {
      title: 'Redux',
      url: 'https://redux.js.org',
      author: 'Andrew Clark',
      num_comments: 2,
      points: 5,
      id: 1
    }
  ];

  // const [searchTerm, setSearchTerm] = useSemiPersistentState('search', 'React');
  const [stories, setStories] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  
  useEffect(() => {
    setIsLoading(true);

    getAsyncStories().then(result => {
      setStories(result.data.stories)
      setIsLoading(false);
    }).catch(() => setIsError(true));
  }, []);

  const getAsyncStories = () =>
    new Promise((resolve) => 
      setTimeout(
        () => resolve({data:{stories: initialStories}}),
        1500
  ));

  const handleRemoveStory = (item) => {
    const newStories = stories.filter((story) => item.id !== story.id);
    setStories(newStories);
  };

  const handleSearch = (e) => {
    setSearchTerm(e.target.value)
  }

  const searchedStories = stories.filter((story) => 
    story.title.toLowerCase().includes(searchTerm.toLowerCase())
  )


  return (
    <div >
      <h1>Road to React</h1>

      <InputWithLabel id='search' value={searchTerm} onInputChange={handleSearch} isFocused>
       <strong>Search:</strong>
      </InputWithLabel> 

      <Search search={searchTerm} onSearch={handleSearch} />

      <hr />
      {isError && <p>Something went wrong...</p>}
      {isLoading ? (<p>Loading...</p>) : (<List list={searchedStories} onRemoveItem={handleRemoveStory} />)}
    </div>
  );
}

const InputWithLabel = ({ id, type, value, onInputChange, children, isFocused }) => {
  const inputRef = useRef();

  useEffect(() => {
    if(isFocused && inputRef.current) {
      inputRef.current.focus();
    }
  }, [isFocused]);

  return (
    <>
      <label htmlFor={id}>{children}</label>
      &nbsp;
      <input id={id} type={type} value={value} onChange={onInputChange} ref={inputRef}></input>
    </>
  )
}

const Search = ({search, onSearch}) => {
  return (
    <>
      <label htmlFor='search'>Search: {' '}</label>
      <input id='search' value={search} type='text' onChange={onSearch} />
    </>
  )
}

const List = ({list, onRemoveItem}) => (
  <ul>
    {list.map((item) => (
      <Item key={item.id} item={item} onRemoveItem={onRemoveItem}
       />
    ))}
  </ul>
)

const Item = ({item, onRemoveItem}) => {
  const handleRemoveItem = () => {
    onRemoveItem(item);
  }

  return (
    <li>
      <span>
        <a href={item.url}>{item.title}/</a>
      </span>
      <span>{item.author}</span>
      <span>{item.num_comments}</span>
      <span>{item.points}</span>
      <span>
        <button type="button" onClick={() => handleRemoveItem(item)}>Dismiss</button>
      </span>
    </li>
  );
};

export default App;
